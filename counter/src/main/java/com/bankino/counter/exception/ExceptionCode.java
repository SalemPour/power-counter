package com.bankino.counter.exception;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public enum ExceptionCode {
    AREA_NOTTRUE("10400", "region is not true!", HttpStatus.BAD_REQUEST.value()),
    COUNTER_NOT_EXISTS("10404", "counter does not exist!", HttpStatus.NOT_FOUND.value()),
    NO_CONSUME_EXISTS("11404", "no consume during time!", HttpStatus.NOT_FOUND.value()),
    INVALID_DATA_INPUT("11405", "invalid input!", HttpStatus.BAD_REQUEST.value()),
    CONFIG_ALREADY_EXISTS("11409", "config already exists!", HttpStatus.CONFLICT.value()),
    CONFIG_NOT_EXISTS("11404", "config does not exist!", HttpStatus.NOT_FOUND.value()),
    CONSUME_ALREADY_ADDED("12404", "consume is already exist!", HttpStatus.CONFLICT.value()),
    ;

    private final String code;
    private final String description;
    private final int httpStatus;

    ExceptionCode(String code, String description, int httpStatus) {
        this.code = code;
        this.description = description;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public int getHttpStatus() {
        return httpStatus;
    }


}