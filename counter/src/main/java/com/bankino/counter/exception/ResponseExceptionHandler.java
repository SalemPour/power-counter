package com.bankino.counter.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ExceptionResponse> handelGeneralException(Exception ex) {
        ExceptionResponse response = ExceptionResponse.builder()
                .errorMessages(Collections.singletonList(ex.getMessage())).actionCode("10100").actionMessage("خطای نامشخص!").build();
        log.error(", response: " + response + " exception::" + ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(LocalException.class)
    public ResponseEntity<ExceptionResponse> handleAllException(LocalException exception) {
        ExceptionResponse response = ExceptionResponse.builder().actionCode(exception.getErrorCode()).actionMessage(exception.getMessage())
                .errorMessages(Collections.singletonList(exception.getMessage()))
                .build();
        log.error(" response " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(exception.getHttpStatus()));
    }


    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
        ExceptionResponse response = ExceptionResponse.builder()
                .actionMessage(ExceptionCode.INVALID_DATA_INPUT.getDescription())
                .errorMessages(errors)
                .build();
        log.error(" response " + response + " exception is:" + errors);
        return new ResponseEntity<>(response, headers, status);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ExceptionResponse> handelConstraintException(ConstraintViolationException ex) {

        ExceptionResponse response = ExceptionResponse.builder()
                .errorMessages(Collections.singletonList(ex.getMessage())).actionCode("11100").actionMessage("نقض محدودیت ورودی!").build();
        log.error("response " + response + " exception is:" + ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}