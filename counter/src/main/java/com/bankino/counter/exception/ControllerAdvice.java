package com.bankino.counter.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;

@Slf4j
@RestControllerAdvice
public class ControllerAdvice {


    @ExceptionHandler(BindException.class)
    public final ResponseEntity<ExceptionResponse> handleBindException(Exception ex) {
        ExceptionResponse response = ExceptionResponse.builder()
                .errorMessages(Collections.singletonList(
                        ex.getLocalizedMessage().substring(ex.getLocalizedMessage().lastIndexOf("default message ["))
                )).actionCode("11400").actionMessage("نقض محدودیت ورودی!").build();
        log.error( " exception is:" + ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
