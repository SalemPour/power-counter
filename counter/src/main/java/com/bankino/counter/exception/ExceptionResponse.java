package com.bankino.counter.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
class ExceptionResponse {
    private String actionMessage;
    private String actionCode;
    List<String> errorMessages;

    @Override
    public String toString() {
        return " ExceptionResponse{" +
                "actionMessage='" + actionMessage + '\'' +
                ", actionCode='" + actionCode + '\'' +
                ", errorMessages=" + errorMessages +
                '}';
    }
}
