package com.bankino.counter.exception;

import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class LocalException extends RuntimeException {
    private int httpStatus;
    private String errorCode;

    public LocalException(String message) {
        super(message);
    }

    public LocalException(ExceptionCode ex) {
        super(ex.getDescription());
        this.httpStatus = ex.getHttpStatus();
        this.errorCode = ex.getCode();
    }

    public LocalException(String message, int httpStatus, String errorCode) {
        super(message);
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
    }

}

