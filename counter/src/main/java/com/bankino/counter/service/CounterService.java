package com.bankino.counter.service;

import com.bankino.counter.controller.dto.CounterInput;
import com.bankino.counter.entity.Counter;
import com.bankino.counter.exception.ExceptionCode;
import com.bankino.counter.exception.LocalException;
import com.bankino.counter.repository.CounterRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
//@Transactional
public class CounterService {
    private final CounterRepository repository;

    public CounterService(CounterRepository counterRepository) {
        this.repository = counterRepository;
    }

    @Transactional(rollbackFor = Throwable.class, isolation = Isolation.REPEATABLE_READ)
    public Object addCounter(CounterInput input) {
        //search if counter is not exists
        Counter counter = new Counter(input);
        counter = repository.save(counter);
        Long id = counter.getId();
        Map<String, Object> result = new HashMap<>();
        result.put("counterId", id);
        result.put("traceNumber", input.getTraceNumber());
        return result;
    }

    @Transactional
    public Map<String, Object> getCounter(Long id) {
        Optional<Counter> optionalCounter = repository.findByIdAndEnable(id, Boolean.TRUE);
        if (!optionalCounter.isPresent())
            throw new LocalException(ExceptionCode.COUNTER_NOT_EXISTS);
        Counter counter = optionalCounter.get();
        Map<String, Object> result = new HashMap<>();
        result.put("counter", counter);
        return result;
    }

    @Transactional
    public Map<String, Object> deleteCounter(Long id) {
        Optional<Counter> optionalCounter = repository.findByIdAndEnable(id, Boolean.TRUE);
        if (!optionalCounter.isPresent())
            throw new LocalException(ExceptionCode.COUNTER_NOT_EXISTS);
        Counter counter = optionalCounter.get();
        repository.delete(counter);
        Map<String, Object> result = new HashMap<>();
        result.put("message", "deleted sucessfull!");
        return result;
    }
}
