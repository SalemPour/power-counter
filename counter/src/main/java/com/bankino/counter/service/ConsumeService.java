package com.bankino.counter.service;

import com.bankino.counter.controller.dto.ConsumeInput;
import com.bankino.counter.entity.ConsumeConfig;
import com.bankino.counter.entity.Counter;
import com.bankino.counter.entity.Consume;
import com.bankino.counter.exception.ExceptionCode;
import com.bankino.counter.exception.LocalException;
import com.bankino.counter.repository.ConsumeConfigRepository;
import com.bankino.counter.repository.CounterRepository;
import com.bankino.counter.repository.ConsumeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
public class ConsumeService {
    private final ConsumeRepository repository;
    private final CounterRepository counterRepository;
    private final ConsumeConfigRepository configRepository;

    public ConsumeService(ConsumeRepository repository, CounterRepository counterRepository, ConsumeConfigRepository configRepository) {
        this.repository = repository;
        this.counterRepository = counterRepository;
        this.configRepository = configRepository;
    }

    @Transactional
    public Object putTransactions(ConsumeInput input) {
        List<Consume> existenceConsume = repository.findExistenceConsume(input.getCounterId(), input.getConsumeDate().toLocalDate(), LocalTime.parse(input.getFromTime()), LocalTime.parse(input.getToTime()));
        if (!existenceConsume.isEmpty())
            throw new LocalException(ExceptionCode.CONSUME_ALREADY_ADDED);
        Optional<Counter> optionalCounter = counterRepository.findById(input.getCounterId());
        if (!optionalCounter.isPresent()) {
            throw new LocalException(ExceptionCode.COUNTER_NOT_EXISTS);
        }
        Counter counter = optionalCounter.get();
        String period = input.getConsumeDate().getMonth() + "" + input.getConsumeDate().getYear();
        List<ConsumeConfig> configs = configRepository.findConfigByParams(counter.getArea(), input.getConsumeDate().toLocalDate(), input.getConsumeDate().toLocalDate(),
                LocalTime.parse(input.getFromTime()), LocalTime.parse(input.getToTime()));
        if (configs.isEmpty())
            configs = configRepository.findOnlyByDateTime(input.getConsumeDate().toLocalDate()
                    , LocalTime.parse(input.getFromTime()));
        if (configs.isEmpty())
            throw new LocalException(ExceptionCode.CONFIG_NOT_EXISTS);
        Optional<ConsumeConfig> first = configs.stream().filter(x -> x.getArea().equals(counter.getArea())).findFirst();
        ConsumeConfig config;
        if (first.isPresent()) {
            config = first.get();
        } else config = configs.get(0);
        Long amountPerKWH = config.getAmountPerKWH();
        long consumption = input.getConsumeAmount() * amountPerKWH;
        Consume consume = new Consume(period, input.getConsumeDate().toLocalDate(), LocalTime.parse(input.getFromTime()), LocalTime.parse(input.getToTime()), counter, consumption);
        repository.save(consume);

        Map<String, String> map = new HashMap<>();
        map.put("message", "consume added successful!");
        return map;
    }

    @Transactional
    public Object getConsumes(Long counterId, LocalDateTime fromDate, LocalDateTime toDate) {
        if (!counterRepository.findById(counterId).isPresent()) {
            throw new LocalException(ExceptionCode.COUNTER_NOT_EXISTS);
        }
        if (toDate == null)
            toDate = LocalDateTime.now();//todo use default value in dto
        List<Consume> consumes = repository.findByCounter_IdAndConsumeDateIsBetween(counterId, fromDate.toLocalDate(), toDate.toLocalDate());
        if (consumes.isEmpty()) {
            throw new LocalException(ExceptionCode.NO_CONSUME_EXISTS);
        }

        Map<String, List<Consume>> result = new HashMap<>();
        result.put("result", consumes);
        return result;
    }

    @Transactional
    public Map<String, List<Long>> getAllCounters(LocalDateTime fromDate, LocalDateTime toDate) {
        List<Long> counterIdList = repository.findAllConsumeCounters(fromDate, toDate);
        if (counterIdList.isEmpty())
            throw new LocalException(ExceptionCode.NO_CONSUME_EXISTS);
        Map<String, List<Long>> result = new HashMap<>();
        result.put("counterIdList", counterIdList);
        return result;
    }

}
