package com.bankino.counter.service;

import com.bankino.counter.controller.dto.ConsumeConfigInput;
import com.bankino.counter.entity.ConsumeConfig;
import com.bankino.counter.exception.ExceptionCode;
import com.bankino.counter.exception.LocalException;
import com.bankino.counter.repository.ConsumeConfigRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConsumeConfigService {
    private final ConsumeConfigRepository repository;

    public ConsumeConfigService(ConsumeConfigRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public Object createConfig(ConsumeConfigInput input) {
        List<ConsumeConfig> existConfig = repository.findConfigByParams(input.getArea(), input.getFromDate(), input.getToDate(),LocalTime.parse(input.getFromTime()), LocalTime.parse(input.getToTime()));
        if (!existConfig.isEmpty()) {
            throw new LocalException(ExceptionCode.CONFIG_ALREADY_EXISTS);
        }
        ConsumeConfig consumeConfig = new ConsumeConfig(input.getArea(), input.getFromDate(), input.getToDate(), input.getMoreThanKWH(), LocalTime.parse(input.getFromTime()), LocalTime.parse(input.getToTime()), input.getAmountPerKWH());
        repository.save(consumeConfig);
        Map<String,Object> result = new HashMap<>();
        result.put("message", " config added successful!");
        return result;
    }
}
