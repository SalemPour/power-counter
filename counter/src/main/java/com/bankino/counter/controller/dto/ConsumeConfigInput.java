package com.bankino.counter.controller.dto;

import com.bankino.counter.entity.enums.AreaEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class ConsumeConfigInput {
    @NotNull(message = "area should enter")
    private AreaEnum area;
    @NotNull(message = "fromDate should enter")
    private LocalDate fromDate;
    @NotNull(message = "toDate should enter")
    private LocalDate toDate;
    @NotNull(message = "moreThanKWH should enter")
    private Long moreThanKWH;
//    @NotNull(message = "fromTime should enter")
    private String fromTime;
////    @NotNull(message = "toTime should enter")
    private String toTime;
    @NotNull(message = "amountPerKWH should enter")
    private Long amountPerKWH;// be ezaye har kiloo what chand rial?
}
