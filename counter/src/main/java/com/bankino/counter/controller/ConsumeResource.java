package com.bankino.counter.controller;

import com.bankino.counter.controller.dto.ConsumeInput;
import com.bankino.counter.service.ConsumeService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(path = "/consumes")
public class ConsumeResource {
    private final ConsumeService service;

    public ConsumeResource(ConsumeService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<Object> saveConsume(@Valid @RequestBody ConsumeInput input){
        Object o = service.putTransactions(input);
        return ResponseEntity.status(HttpStatus.CREATED).body(o);
    }
    @GetMapping(path = "{counterId}")
    public ResponseEntity<Object> getConsumes(@PathVariable(value = "counterId", required = false) @NotNull(message = "id should not be null") Long id,
                                              @RequestParam  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fromDate, @RequestParam(required = false)     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime toDate) {
        Object consumes = service.getConsumes(id, fromDate, toDate);
        return ResponseEntity.ok(consumes);
    }

    @GetMapping(path = "/allCounterId")//to get all counters who have consumes during time
    public ResponseEntity<Object> getAllCounterIdList(@RequestParam     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime fromDateTime,@RequestParam     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime toDateTime)
    {
        Map<String, List<Long>> allCounters = service.getAllCounters(fromDateTime, toDateTime);
        return ResponseEntity.ok(allCounters);
    }
}
