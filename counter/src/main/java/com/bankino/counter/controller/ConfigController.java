package com.bankino.counter.controller;

import com.bankino.counter.controller.dto.ConsumeConfigInput;
import com.bankino.counter.service.ConsumeConfigService;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@Validated
@RequestMapping(path = "/configs", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConfigController {
    private final ConsumeConfigService service;

    public ConfigController(ConsumeConfigService service) {
        this.service = service;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createConfig(@Valid @RequestBody ConsumeConfigInput input) {
        Object config = service.createConfig(input);
        return ResponseEntity.status(HttpStatus.CREATED).body(config);
    }
}
