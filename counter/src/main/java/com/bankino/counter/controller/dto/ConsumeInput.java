package com.bankino.counter.controller.dto;

import lombok.Data;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ConsumeInput implements Serializable {
    private static final long serialVersionUID = 3970009476069112924L;
    @NotNull(message = "counterid should enter")
    private Long counterId;
    @NotNull(message = "consume date should enter!")
    private LocalDateTime consumeDate;
    @NotNull(message = " fromTime should enter!")
    @Pattern(regexp = "(\\d{2})\\:(\\d{2})\\:(\\d{2})", message = "fromTime format is not true!")
    private String fromTime;
    @NotNull(message = "totime should enter")
    @Pattern(regexp = "(\\d{2})\\:(\\d{2})\\:(\\d{2})", message = "toTime format is not true!")
    private String toTime;
    @NotNull(message = "consumeAmount should not be null")
    private Long consumeAmount;//kwh
}
