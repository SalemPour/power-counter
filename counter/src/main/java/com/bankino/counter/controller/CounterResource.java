package com.bankino.counter.controller;

import com.bankino.counter.controller.dto.CounterInput;
import com.bankino.counter.service.CounterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Validated
@RequestMapping(path = "/counters", produces = MediaType.APPLICATION_JSON_VALUE)
public class CounterResource {
    private final CounterService servicec;

    public CounterResource(CounterService servicec) {
        this.servicec = servicec;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> insertCounter(@Valid @RequestBody CounterInput input) {
        Object o = servicec.addCounter(input);
        return ResponseEntity.status(HttpStatus.CREATED).body(o);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Object> getCounter(@PathVariable(value = "id", required = false) @NotNull(message = "id should not be null") Long id) {
        Object o = servicec.getCounter(id);
        return ResponseEntity.status(HttpStatus.OK).body(o);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteCounter(@PathVariable(value = "id", required = false) @NotNull(message = "id should not be null") Long id) {
        Object o = servicec.deleteCounter(id);
        return ResponseEntity.status(HttpStatus.OK).body(o);
    }

    }


