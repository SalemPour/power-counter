package com.bankino.counter.controller.dto;

import com.bankino.counter.entity.enums.AreaEnum;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class CounterInput implements Serializable {
    @NotNull(message = "area should enter")
    private AreaEnum area;

    @NotBlank(message = "postalCode should enter")
    private String postalCode;

//    @NotBlank(message = "address should not be blank or null!")
//    private String address;

    @NotBlank(message = "address should not be blank or null!")
    @Size(min = 4, max = 36, message = "trace number size is not true!")
    private String traceNumber;

}
