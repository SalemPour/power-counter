package com.bankino.counter.repository;

import com.bankino.counter.entity.enums.AreaEnum;
import com.bankino.counter.entity.ConsumeConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface ConsumeConfigRepository extends JpaRepository<ConsumeConfig, Long> {
    @Query("select C from ConsumeConfig C where C.area= :area and C.fromDate>= :fromDate and C.toDate<= :toDate and C.fromTime>= :fromTime and C.toTime <= :toTime ")
    List<ConsumeConfig> findConfigByParams(@Param("area") AreaEnum area, @Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("fromTime") LocalTime fromTime, @Param("toTime") LocalTime toTime);

    List<ConsumeConfig> findByArea(AreaEnum area);
    @Query("select C from ConsumeConfig C where C.fromDate<= :consumeDate and C.toDate>= :consumeDate and C.fromTime<= :time and C.toTime >= :time ")
    List<ConsumeConfig> findOnlyByDateTime(@Param("consumeDate") LocalDate fromDate ,@Param("time")LocalTime toTime);
}
