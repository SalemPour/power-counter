package com.bankino.counter.repository;

import com.bankino.counter.entity.Counter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CounterRepository extends JpaRepository<Counter, Long> {
    Optional<Counter> findByIdAndEnable(Long id, Boolean isEnable);
}
