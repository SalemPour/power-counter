package com.bankino.counter.repository;

import com.bankino.counter.entity.Consume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface ConsumeRepository extends JpaRepository<Consume, Long> {
    List<Consume> findByCounter_IdAndConsumeDateIsBetween(Long counterId, LocalDate fromDate, LocalDate toDate);

    @Query("select C.id from Counter C where C.createDate>= :fromDate and C.createDate<= :toDate")
    List<Long> findAllConsumeCounters(@Param("fromDate") LocalDateTime fromDate, @Param("toDate") LocalDateTime toDate);

    @Query("select C from Consume C where C.counter.id= :counterId and C.consumeDate= :consumeDate and C.fromTime= :fromTime and C.toTime= :toTime")
    List<Consume> findExistenceConsume(Long counterId, LocalDate consumeDate, LocalTime fromTime, LocalTime toTime);
}
