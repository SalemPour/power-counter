package com.bankino.counter.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "consume")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Consume {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "counter_sequence", sequenceName = "counter_sequence", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counter_sequence")
    private Long id;
    @Column(name = "amount")
    private Long amount;
    @Column(name = "description")
    private String description;
    @Column(name = "period")
    private String period;

    @Column(name = "consumedate")
    private LocalDate consumeDate;


    @Column(name = "fromtime")
    private LocalTime fromTime;
    @Column(name = "totime")
    private LocalTime toTime;
    @Column(name = "consumption")
    private Long consumption;

    @Column(name = "paydate")
    private LocalDateTime registerDate =LocalDateTime.now();
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "counterid", referencedColumnName = "counterid")
    private Counter counter;


    public Consume(String period, LocalDate consumeDate, LocalTime fromTime, LocalTime toTime, Counter counter, Long consumption) {
        this.period = period;
        this.consumeDate = consumeDate;
        this.toTime = toTime;
        this.fromTime = fromTime;
        this.counter = counter;
        this.consumption = consumption;

    }
}
