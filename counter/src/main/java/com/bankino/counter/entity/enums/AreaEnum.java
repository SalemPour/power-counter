package com.bankino.counter.entity.enums;

import com.bankino.counter.exception.ExceptionCode;
import com.bankino.counter.exception.LocalException;
import org.springframework.http.HttpStatus;

public enum AreaEnum {
    VELENJAK(1, "NORTH"), SAFRANIE(2, "NORTH"), TEHRANPARS(3, "EAST"), AJODANIEH(4, "NORTH"), MEHRABAD(5, "WEST"), POUNAK(6, "WEST"), RAHAHAN(7, "SOUTRH"), AZADI(8, "WEST");
    public int value;
    public String region;

    AreaEnum(int i, String region) {
        this.value = i;
        this.region = region;
    }

    public static AreaEnum fromValue(Integer value) {
        switch (value) {
            case 1:
                return VELENJAK;
            case 2:
                return SAFRANIE;
            case 3:
                return TEHRANPARS;
            case 4:
                return AJODANIEH;
            case 5:
                return MEHRABAD;
            case 6:
                return POUNAK;
            case 7:
                return RAHAHAN;
            case 8:
                return AZADI;
            default:
                throw new LocalException(ExceptionCode.AREA_NOTTRUE.getDescription(),
                        HttpStatus.NOT_FOUND.value(), ExceptionCode.AREA_NOTTRUE.getCode());
        }
    }
}
