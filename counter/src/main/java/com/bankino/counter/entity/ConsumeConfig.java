package com.bankino.counter.entity;

import com.bankino.counter.entity.enums.AreaEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "consumeconfig")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "configId")
public class ConsumeConfig {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "counter_sequence", sequenceName = "counter_sequence", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counter_sequence")
    private Long configId;

    @Column(name = "area")
    @Enumerated(EnumType.STRING)
    private AreaEnum area;

    @Column(name = "fromdate")
    private LocalDate fromDate;

    @Column(name = "todate")
    private LocalDate toDate;

    @Column(name = "morethankwh")
    private Long moreThanKWH;

    @Column(name = "fromtime")
    private LocalTime fromTime;

    @Column(name = "totime")
    private LocalTime toTime;

    @Column(name = "amountperkwh")
    private Long amountPerKWH;

    public ConsumeConfig(AreaEnum area, LocalDate fromDate, LocalDate toDate, Long moreThanKWH, LocalTime fromTime, LocalTime toTime, Long amountPerKWH) {
        this.area = area;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.moreThanKWH = moreThanKWH;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.amountPerKWH = amountPerKWH;
    }
}
