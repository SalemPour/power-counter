package com.bankino.counter.entity;

import com.bankino.counter.controller.dto.CounterInput;
import com.bankino.counter.entity.enums.AreaEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "counter")
@Getter
@Setter
public class Counter {
    @Id
    @Column(name = "counterid")
    @SequenceGenerator(name = "counter_sequence", sequenceName = "counter_sequence", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "counter_sequence")
    private Long id;// shenase counter

    @Column(name = "region")
    private String region;

    @Column(name = "area")
    @Enumerated(EnumType.STRING)
    private AreaEnum area;

    @Column(name = "enable")
    private boolean enable = true;

    @Column(name = "address")
    private String address;

    @Column(name = "postalcode")
    private String postalCode;


    @Column(name = "traceno")
    private String traceNumber;

    @Column(name = "createdate")
    private LocalDateTime createDate = LocalDateTime.now();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "counter", fetch = FetchType.EAGER)
    @ToString.Exclude
    @JsonIgnore
    List<Consume> transactions = new ArrayList<>();

    public Counter() {
    }

    public Counter(CounterInput input) {
        AreaEnum area = input.getArea();
        this.region = area.region;
        this.address = area.name();
        this.postalCode = input.getPostalCode();
        this.area = area;
        this.traceNumber = input.getTraceNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Counter counter = (Counter) o;
        return enable == counter.enable &&
                Objects.equals(id, counter.id) &&
                Objects.equals(region, counter.region) &&
                Objects.equals(address, counter.address) &&
                Objects.equals(postalCode, counter.postalCode) &&
                Objects.equals(area, counter.area);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, region, enable, address, postalCode, area);
    }
}
