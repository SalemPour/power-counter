package com.bankino.messaging.controller;

import com.bankino.messaging.service.MailService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/mails", produces = MediaType.APPLICATION_JSON_VALUE)
public class MailController {
    private final MailService service;

    public MailController(MailService service) {
        this.service = service;
    }

    @PostMapping

    public ResponseEntity<Object> sendMail(@RequestParam String text, @RequestParam String to, @RequestParam String subject) {
        service.sendEmail(to, subject, text);
        return ResponseEntity.ok(new HashMap<>().put("name", "mailSent"));
    }
}
