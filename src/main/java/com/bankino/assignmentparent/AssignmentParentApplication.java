package com.bankino.assignmentparent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentParentApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentParentApplication.class, args);
    }

}
