package com.bankino.schedduling.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class RestClient {
    public Object getAllCounters(LocalDateTime fromDate, LocalDateTime toDate) throws UnirestException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        HttpResponse<String> response;
        String uri = "http://localhost:8282/consumes/allCounterId";
        Object responseBody = null;
        log.info(", request to counter, uri:" + uri);
        try {
            response = Unirest.get(uri).queryString("fromDateTime", fromDate).queryString("toDateTime", toDate)
                    .asString();
            String body = response.getBody();
            log.info(" response  http status:" + response.getStatus());
        } catch (UnirestException e) {
            log.error(" error response :" + e.getMessage());
            throw e;
        }
        return responseBody;

    }

    public Object sendCounterInfo(Long id) throws JsonProcessingException, UnirestException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        HttpResponse<String> settlementResponse;
        String uri = "http://localhost:8282/counters/" + id;
        Object responseBody = null;
        log.info(", request to counter, uri:" + uri);
        try {
            settlementResponse = Unirest.get(uri)
                    .header("Content-Type", "application/json")
                    .asString();
            log.info(" response  http status:" + settlementResponse.getStatus());
        } catch (UnirestException e) {
            log.error(" error response :" + e.getMessage());
            throw e;
        }
        return responseBody;


    }
}

