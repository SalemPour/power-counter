package com.bankino.schedduling.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "consmequeue")
@Data
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class ConsumeQueueEntity {
    @Id
    @SequenceGenerator(name = "queue_sequence", sequenceName = "queue_sequence", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "queue_sequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "createdate")
    private LocalDateTime createDate = LocalDateTime.now();
    @Column(name = "amount")
    private Long amount;//mablaghe hasel

    @Column(name = "counterid")
    private Long counterId;

    @Column(name = "fromdate")
    private LocalDateTime fromDate;

    @Column(name = "todate")
    private LocalDateTime toDate;

    public ConsumeQueueEntity( Long amount, Long counterId, LocalDateTime fromDate, LocalDateTime toDate) {
        this.amount = amount;
        this.counterId = counterId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
