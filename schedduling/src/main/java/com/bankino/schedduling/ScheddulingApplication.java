package com.bankino.schedduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheddulingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScheddulingApplication.class, args);
    }

}
