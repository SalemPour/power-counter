package com.bankino.schedduling.repository;

import com.bankino.schedduling.Entity.ConsumeQueueEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionQueueRepository extends JpaRepository<ConsumeQueueEntity,Long> {
}
