package com.bankino.schedduling.job;

import com.bankino.schedduling.Entity.ConsumeQueueEntity;
import com.bankino.schedduling.client.RestClient;
import com.bankino.schedduling.repository.TransactionQueueRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TransactionJob {
    private final RestClient restClient;
    private final TransactionQueueRepository repository;

    public TransactionJob(RestClient rest, TransactionQueueRepository repository) {
        this.restClient = rest;
        this.repository = repository;
    }

//        @Scheduled(cron = "0 0 0/1 * * *")//every 1 hours
    @Scheduled(cron = "0/5 * * * * *")// every5 seconds
    @Transactional
    public void sendRequest() throws JsonProcessingException, UnirestException {
            Object allCounters = restClient.getAllCounters(LocalDateTime.now().minusHours(1), LocalDateTime.now());
//            Object allCounters = restClient.getAllCounters(LocalDateTime.now().minusDays(2).minusHours(10), LocalDateTime.now());

            if (allCounters==null)
                return;
            List<Long> counterIdList = new ArrayList<>();//todo
            counterIdList.add(0,1L);
            new ConsumeQueueEntity(/*amount*/1l,counterIdList.get(0),/*fromDate*/LocalDateTime.now().minusHours(4),LocalDateTime.now()/*todo*/);



//            for (Long id : allCounters) {
//            Object o = restClient.sendCounterInfo(id);
//        }

    }
}
